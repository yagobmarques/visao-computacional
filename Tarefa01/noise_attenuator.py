import cv2 
import numpy as np

def media(a1, a2, a3, a4, a5, a6, a7, a8, a9):
  return (a1+a2+a3+a4+a5+a6+a7+a8+a9)/9

a1 = cv2.imread("a1.jpg").astype("uint16")
a2 = cv2.imread("a2.jpg").astype("uint16")
a3 = cv2.imread("a3.jpg").astype("uint16")
a4 = cv2.imread("a4.jpg").astype("uint16")
a5 = cv2.imread("a5.jpg").astype("uint16")
a6 = cv2.imread("a6.jpg").astype("uint16")
a7 = cv2.imread("a7.jpg").astype("uint16")
a8 = cv2.imread("a8.jpg").astype("uint16")
a9 = cv2.imread("a9.jpg").astype("uint16")

noiseless_img = np.zeros(a1.shape) 

for i in range (a1.shape[0]):
  for j in range(a1.shape[1]):
    b = media(a1[i][j][0], a2[i][j][0], a3[i][j][0], a4[i][j][0], a5[i][j][0], a6[i][j][0], a7[i][j][0], a8[i][j][0], a9[i][j][0])
    g = media(a1[i][j][1], a2[i][j][1], a3[i][j][1], a4[i][j][1], a5[i][j][1], a6[i][j][1], a7[i][j][1], a8[i][j][1], a9[i][j][1])
    r = media(a1[i][j][2], a2[i][j][2], a3[i][j][2], a4[i][j][2], a5[i][j][2], a6[i][j][2], a7[i][j][2], a8[i][j][2], a9[i][j][2])
    noiseless_img[i][j]= [b, g, r]

cv2.imwrite("noiseless.jpg", noiseless_img)