import cv2 
import numpy as np
## O CSV usa o padrão BGR
name_img = input("Digite o caminho da imagem: ")
img = cv2.imread(name_img)
img_B = img.copy()
img_G = img.copy()
img_R = img.copy()
# tirando os canais
img_R[:,:,[0, 1]]= 0 
img_B[:,:,[1, 2]]= 0
img_G[:,:,[0, 2]]= 0
cv2.imwrite("red.jpg", img_R)
cv2.imwrite("blue.jpg", img_B)
cv2.imwrite("green.jpg", img_G)
print("Os canais RGB foram separados! Estão nos arquivos red.jpg, blue.jpg e green.jpg")