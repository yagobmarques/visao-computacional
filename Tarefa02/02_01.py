import cv2
import numpy as np

def convolucao(amostra, kernel, num):
  sum = 0
  for i in range(amostra.shape[0]):
    for j in range(amostra.shape[1]):
      sum += amostra[i][j] * kernel[i][j]
  return abs(sum)/num

img = cv2.imread("foto-perfil.jpg", 0).astype("uint16")

len_black_strip = 5

## Gamma Correction
gamma = 3.0
invgamma = 1.0/gamma
table = dict()
for i in range(0, 256):
  table[i] =  (i/255) ** invgamma
for i in range(img.shape[0]):
  for j in range(img.shape[1]):
    img[i][j] = table[img[i][j]] * 255

cv2.imwrite("gama_correction.jpg", img)

## Black Strip
for j in range(0, img.shape[1], 23):
  black_strip = j+len_black_strip
  if black_strip>img.shape[1]:
    black_strip = img.shape[1]
  img[:,j:black_strip] = 0
## End black_strip  

cv2.imwrite("stripped.jpg", img)

## Median Filter
ordem = int(input("Ordem da mediana (apenas ímpares): "))
if ordem % 2 == 0:
  ordem += 1
n_esimo = int((ordem + 1)/2)

kernel = np.zeros((ordem,ordem), dtype="uint16")
kernel = np.add(kernel, 1)
end_image = np.zeros(img.shape, dtype="uint16")

for i in range(n_esimo-1, img.shape[0]- n_esimo+1):
  for j in range(n_esimo-1, img.shape[1] - n_esimo+1):
    end_image[i][j] = convolucao(img[i-n_esimo+1:i+n_esimo, j-n_esimo+1:j+n_esimo], kernel, ordem*ordem)
cv2.imwrite("final_image.jpg", end_image)