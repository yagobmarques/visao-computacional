import cv2
import numpy as np

def convolucao(amostra, kernel, num):
  sum = 0
  for i in range(amostra.shape[0]):
    for j in range(amostra.shape[1]):
      sum += amostra[i][j] * kernel[i][j]
  return abs(sum)/num

img = cv2.imread("foto-perfil.jpg", 0).astype("uint16")

cv2.imwrite("grey.jpg", img)

## Prewitte
kernel_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]], dtype = "float" )
kernel_y = np.array([[1, 1, 1], [0, 0, 0], [-1, -1, -1]], dtype = "float" )
gradient = np.zeros(img.shape, dtype="float")
for i in range(img.shape[0]):
  for j in range(img.shape[1]):
    grad_x = convolucao(img[i-1:i+2, j-1:j+2], kernel_x, 1)
    grad_y = convolucao(img[i-1:i+2, j-1:j+2], kernel_y, 1)
    gradient[i][j] = (grad_x ** 2 + grad_y ** 2) ** 0.5

## Median Filter 

ordem = int(input("Ordem da mediana (apenas ímpares): "))
if ordem % 2 == 0:
  ordem += 1
n_esimo = int((ordem + 1)/2)

limiar = int(input("Entre com o Limiar(int): "))

kernel = np.zeros((ordem,ordem), dtype="uint16")
kernel = np.add(kernel, 1)
end_image = np.zeros(img.shape, dtype="uint16")

for i in range(n_esimo-1, img.shape[0]- n_esimo+1):
  for j in range(n_esimo-1, img.shape[1] - n_esimo+1):
    if gradient[i][j]<limiar:
      end_image[i][j] = convolucao(img[i-n_esimo+1:i+n_esimo, j-n_esimo+1:j+n_esimo], kernel, ordem*ordem)
    else:
      end_image[i][j]= img[i][j]

cv2.imwrite("teste"+str(limiar)+"media"+str(ordem)+".jpg", end_image)