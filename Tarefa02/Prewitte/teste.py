import cv2
import numpy as np

def convolucao(amostra, kernel, num):
  sum = 0
  for i in range(amostra.shape[0]):
    for j in range(amostra.shape[1]):
      sum += amostra[i][j] * kernel[i][j]
  return abs(sum)/num

img = cv2.imread("foto-perfil.jpg", 0).astype("uint16")

cv2.imwrite("grey.jpg", img)


## Prewitte
kernel_x = np.array([[-1, 0, 1], [-1, 0, 1], [-1, 0, 1]], dtype = "float" )
kernel_y = np.array([[1, 1, 1], [0, 0, 0], [-1, -1, -1]], dtype = "float" )
gradient = np.zeros(img.shape, dtype="float")
for i in range(img.shape[0]):
  for j in range(img.shape[1]):
    grad_x = convolucao(img[i-1:i+2, j-1:j+2], kernel_x, 1)
    grad_y = convolucao(img[i-1:i+2, j-1:j+2], kernel_y, 1)
    gradient[i][j] = (grad_x ** 2 + grad_y ** 2) ** 0.5

end_image = np.zeros(img.shape, dtype="uint16")
end_image = np.add(end_image, 256)
#limiar = int(input("Entre com o Limiar(int): "))
for limiar in range (0, 255, 15):
    for i in range(0, img.shape[0]):
        for j in range(0, img.shape[1]):
            if gradient[i][j]<limiar:
                end_image[i][j] = 255
            else:
                end_image[i][j]= 0
    cv2.imwrite("prewitte"+str(limiar)+".jpg", end_image)